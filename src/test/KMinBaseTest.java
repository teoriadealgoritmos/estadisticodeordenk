package test;

import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import algoritmos.KMinimoInterface;
import exceptions.NotValidKException;

public abstract class KMinBaseTest {
	
	protected KMinimoInterface algoritmo;
	
	@Test
	public void testArrayNulo() {
		int array[] = null;
		try {
			algoritmo.obtenerKElementoMinimo(array, 1);
			fail("Se esperaba excepcion NullPointerException");
		} catch (NullPointerException e) {
		} catch (NotValidKException e) {
			fail("Se esperaba excepcion NullPointerException");
		}
	}
	
	@Test
	public void testKNoValido() {
		int array[] = new int[6];
		array[0] = 5;
		try {
			algoritmo.obtenerKElementoMinimo(array, -1);
			fail("Se esperaba excepcion NotValidKException");
		} catch (NotValidKException e) {
		}
	}
	
	@Test
	public void testKNoValido2() {
		int array[] = new int[2];
		array[0] = 5;
		array[1] = 4;
		try {
			algoritmo.obtenerKElementoMinimo(array, 2);
			fail("Se esperaba excepcion NotValidKException");
		} catch (NotValidKException e) {
		}
	}

	@Test
	public void testUnElemento() {
		int array[] = new int[1];
		array[0] = 5;
		try {
			int resultado = algoritmo.obtenerKElementoMinimo(array, 0);
			Assert.assertEquals(5, resultado);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}
	
	@Test
	public void testElementoDelMedio() {
		int array[] = new int[3];
		array[0] = 5;
		array[1] = 3;
		array[2] = 6;
		try {
			int resultado = algoritmo.obtenerKElementoMinimo(array, 1);
			Assert.assertEquals(5, resultado);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}
	
	@Test
	public void testUltimoElemento() {
		int array[] = new int[4];
		array[0] = 5;
		array[1] = 3;
		array[2] = 6;
		array[3] = 2;
		try {
			int resultado = algoritmo.obtenerKElementoMinimo(array, 3);
			Assert.assertEquals(6, resultado);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}
	
	@Test
	public void testArrayGrandeKChico() {
		int array1[] = new int[10000];
		int array2[] = new int[10000];
		int array3[] = new int[10000];
		int array4[] = new int[10000];
		int array5[] = new int[10000];
		Random numeroAletorio = new Random();
		for (int i = 0; i < array1.length; i++) {
			array1[i] = numeroAletorio.nextInt(1000);
			array2[i] = numeroAletorio.nextInt(1000);
			array3[i] = numeroAletorio.nextInt(1000);
			array4[i] = numeroAletorio.nextInt(1000);
			array5[i] = numeroAletorio.nextInt(1000);
		}
		try {
			algoritmo.obtenerKElementoMinimo(array1, 2);
			algoritmo.obtenerKElementoMinimo(array2, 2);
			algoritmo.obtenerKElementoMinimo(array3, 2);
			algoritmo.obtenerKElementoMinimo(array4, 2);
			algoritmo.obtenerKElementoMinimo(array5, 2);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}

	@Test
	public void testArrayGrandeKMediano() {
		int array1[] = new int[10000];
		int array2[] = new int[10000];
		int array3[] = new int[10000];
		int array4[] = new int[10000];
		int array5[] = new int[10000];
		Random numeroAletorio = new Random();
		for (int i = 0; i < array1.length; i++) {
			array1[i] = numeroAletorio.nextInt(1000);
			array2[i] = numeroAletorio.nextInt(1000);
			array3[i] = numeroAletorio.nextInt(1000);
			array4[i] = numeroAletorio.nextInt(1000);
			array5[i] = numeroAletorio.nextInt(1000);
		}
		try {
			algoritmo.obtenerKElementoMinimo(array1, 500);
			algoritmo.obtenerKElementoMinimo(array2, 500);
			algoritmo.obtenerKElementoMinimo(array3, 500);
			algoritmo.obtenerKElementoMinimo(array4, 500);
			algoritmo.obtenerKElementoMinimo(array5, 500);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}
	
	@Test
	public void testArrayGrandeKGrande() {
		int array1[] = new int[10000];
		int array2[] = new int[10000];
		int array3[] = new int[10000];
		int array4[] = new int[10000];
		int array5[] = new int[10000];
		Random numeroAletorio = new Random();
		for (int i = 0; i < array1.length; i++) {
			array1[i] = numeroAletorio.nextInt(1000);
			array2[i] = numeroAletorio.nextInt(1000);
			array3[i] = numeroAletorio.nextInt(1000);
			array4[i] = numeroAletorio.nextInt(1000);
			array5[i] = numeroAletorio.nextInt(1000);
		}
		try {
			algoritmo.obtenerKElementoMinimo(array1, 999);
			algoritmo.obtenerKElementoMinimo(array2, 999);
			algoritmo.obtenerKElementoMinimo(array3, 999);
			algoritmo.obtenerKElementoMinimo(array4, 999);
			algoritmo.obtenerKElementoMinimo(array5, 999);
		} catch (NotValidKException e) {
			fail("Se genero una excepcion");
		}
	}
}
