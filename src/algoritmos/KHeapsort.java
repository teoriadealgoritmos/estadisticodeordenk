package algoritmos;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class KHeapsort implements KMinimoInterface {
	
	int cantidadElementos;
	int []vector;
	
	public int obtenerKElementoMinimo(int[] vector, int k) throws NotValidKException, NullPointerException {
		KMinValidator.validateParameters(vector, k);
		cantidadElementos = vector.length-1;
		int valor = 0;
		this.vector = vector;
		int index = ((cantidadElementos/2) > 0) ? (cantidadElementos/2) : 0;
		minHeapify(index);
		for (int i = 0; i < k+1; i++) {
			valor = vector[0];
			vector[0] = vector[cantidadElementos];
			vector[cantidadElementos] = Integer.MAX_VALUE;
			index = ((cantidadElementos/2) > 0) ? (cantidadElementos/2) : 0;
			minHeapify(index);
		}
		return valor;
	}

	public void minHeapify(int indicePadre)
    {
        int hijoIzq = ((indicePadre+1)*2)-1;
        int hijoDer = ((indicePadre+1)*2);
        int indiceMenor = indicePadre;
        if(vector.length > hijoIzq && vector[hijoIzq] < vector[indicePadre])
        {
            indiceMenor = hijoIzq;
        }

        if(vector.length > hijoDer && vector[hijoDer] < vector[indicePadre])
        {
            indiceMenor = hijoDer;
        }
        if(indiceMenor !=indicePadre)
        {
            swap(indicePadre, indiceMenor);
            minHeapify(indiceMenor);
        } else if (indicePadre != 0) {
        	minHeapify(indicePadre / 2);
        }
    }
	
	public void swap(int parentIndex, int smallest) {
		int tmp = vector[parentIndex];
		vector[parentIndex] = vector[smallest];
		vector[smallest] = tmp;
	}
}