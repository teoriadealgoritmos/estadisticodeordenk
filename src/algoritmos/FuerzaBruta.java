package algoritmos;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class FuerzaBruta implements KMinimoInterface{

	
	public int obtenerKElementoMinimo(int[] vector, int k) throws NotValidKException {
		KMinValidator.validateParameters(vector, k);
		
		int min = 0;
		int countMin = -1;
		for (int i = 0; i < vector.length && countMin != k; i++) {
			min = vector[i];
			countMin = 0;
			for (int j = 0; j < vector.length; j++) {
				if (i != j && vector[j] < min) {
					countMin++;
				}
			}
		}
		
		return min;
	}

}
