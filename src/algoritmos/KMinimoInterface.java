package algoritmos;

import exceptions.NotValidKException;

public interface KMinimoInterface {
	
	public int obtenerKElementoMinimo(int[] vector, int k) throws NotValidKException;
}
