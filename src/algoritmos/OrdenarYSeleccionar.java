package algoritmos;

import java.util.Arrays;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class OrdenarYSeleccionar implements KMinimoInterface {

	public int obtenerKElementoMinimo(int vector[], int k)
			throws NotValidKException, NullPointerException {
		KMinValidator.validateParameters(vector, k);
		Arrays.sort(vector);
		return vector[k];
	}
}
