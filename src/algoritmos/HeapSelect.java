package algoritmos;

import java.util.Collections;
import java.util.PriorityQueue;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class HeapSelect implements KMinimoInterface{

	public int obtenerKElementoMinimo(int[] vector, int k) throws NotValidKException {
		KMinValidator.validateParameters(vector, k);
		
		int minimo = -1;
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>(k + 1, Collections.reverseOrder());
		for (Integer item : vector) {
			if (heap.size() < k + 1|| item < heap.peek()) {
				if (heap.size() == k + 1)
					heap.poll();
				heap.offer(item);
			}
		}
		minimo = heap.peek();
		return minimo;
	}

}
