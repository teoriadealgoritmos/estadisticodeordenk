package algoritmos;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class QuickSelect implements KMinimoInterface{
	
	public int obtenerKElementoMinimo(int[] vector, int k) throws NotValidKException {
		KMinValidator.validateParameters(vector, k);
		return quickselect(vector, 0, vector.length - 1, k+1);
	}
	
	public int quickselect(int[] vector, int izquierda, int derecha, int k) {

		if (izquierda == derecha) {
			return vector[izquierda];
		}
		
		int indicePivote = particion(vector, izquierda, derecha);
		int tamanioIzq = indicePivote - izquierda + 1;
		
		if (tamanioIzq == k) {
			return vector[indicePivote];
		} else if (tamanioIzq > k) {
			return quickselect(vector, izquierda, indicePivote - 1, k);
		} else {
			return quickselect(vector, indicePivote + 1, derecha, k - tamanioIzq);
		}
	}

	public int particion(int[] vector, int izquierda, int derecha) {
		int indicePivote = calcularSwap(vector, izquierda, derecha);
		int valorPivote = vector[indicePivote];
		int indiceAlmacenar = izquierda;
		swap(vector, indicePivote, derecha);
		for (int i = izquierda; i < derecha; i++) {
			if (vector[i] <= valorPivote) {
				swap(vector, indiceAlmacenar, i);
				indiceAlmacenar++;
			}
		}
		swap(vector, derecha, indiceAlmacenar);
		return indiceAlmacenar;
	}

	public int calcularSwap(int[] vector, int izquierda, int derecha) {
		int centro = (izquierda + derecha) / 2;
		if (vector[izquierda] > vector[derecha]) {
			swap(vector, izquierda, centro);
		}
		if (vector[izquierda] > vector[derecha]) {
			swap(vector, izquierda, derecha);
		}
		if (vector[centro] > vector[derecha]) {
			swap(vector, centro, derecha);
		}
		swap(vector, centro, derecha - 1);
		return derecha - 1;
	}

	public void swap(int[] vector, int indice1, int indice2) {
		int temp = vector[indice1];
		vector[indice1] = vector[indice2];
		vector[indice2] = temp;
	}
}