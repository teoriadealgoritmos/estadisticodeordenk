package algoritmos;

import exceptions.NotValidKException;
import validations.KMinValidator;

public class KSelecciones implements KMinimoInterface {

	public int obtenerKElementoMinimo(int vector[], int k)
			throws NotValidKException, NullPointerException {
		KMinValidator.validateParameters(vector, k);
		int minimo = 0;
		for (int i = 0; i <= k; i++) {
			minimo = vector[i];
			int indiceDelMinimo = i;
			for (int j = i; j < vector.length; j++) {
				if (vector[j] < minimo) {
					minimo = vector[j];
					indiceDelMinimo = j;
				}
			}
			int aux = vector[i];
			vector[i] = minimo;
			vector[indiceDelMinimo] = aux;
		}
		return minimo;
	}

}
