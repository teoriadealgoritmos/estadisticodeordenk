package validations;

import exceptions.NotValidKException;

public class KMinValidator {
	
	public static void validateParameters(int[] vector, int k) throws NotValidKException {
		if (vector == null) {
			throw new NullPointerException();
		}
		else if (k < 0 || k >= vector.length) {
			throw new NotValidKException();
		}
	}
}
